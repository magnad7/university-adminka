export const actions = {
    // * user 
    getUser(_, { id }) {
        return new Promise(async (resolve, reject) => {
            try {
                const res = await this.$axios.get(`user/${id}`)
                const { status, message, data } = res.data
                if (status) {
                    resolve({
                        status,
                        data,
                        error: null
                    })
                } else {
                    reject({
                        status,
                        error: message
                    })
                }
            } catch (e) {
                reject({
                    status: false,
                    error: e
                })
            }
        })
    },
    updateUser(_, { form, id }) {
        console.log(form, "formmm");
        return new Promise(async (resolve, reject) => {
            try {
                const res = await this.$axios.put(`user/${id}`, form)
                const { status, message } = res.data
                if (status) {
                    resolve({
                        status,
                        error: null
                    })
                } else {
                    reject({
                        status,
                        error: message
                    })
                }
            } catch (e) {
                reject({
                    status: false,
                    error: e
                })
            }
        })
    },
}