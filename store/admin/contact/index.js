export const state = () => ({
    list: [],
    count: 0,
})

export const getters = {
    list: (state) => { return state.list },
    count: (state) => { return state.count },
}

export const mutations = {
    setState(state, { key, value }) {
        state[key] = value
    }
}

export const actions = {
    // * user 
    fetchList({ commit }) {
        return new Promise(async (resolve, reject) => {
            try {
                const res = await this.$axios.get('/contact/list')
                const { status, message, data } = res.data
                if (status) {
                    commit("setState", { key: "list", value: data.results || [] })
                    commit("setState", { key: "count", value: data.count || 0 })
                    resolve({
                        status,
                        error: null
                    })
                } else {
                    reject({
                        status,
                        error: message
                    })
                }
            } catch (e) {
                reject({
                    status: false,
                    error: e
                })
            }
        })
    },
    postDetail(_, form) {
        return new Promise(async (resolve, reject) => {
            try {
                const res = await this.$axios.post('/contact/create', form)
                const { status, message, data } = res.data
                if (status) {
                    resolve({
                        status,
                        data,
                        error: null
                    })
                } else {
                    reject({
                        status,
                        error: message
                    })
                }
            } catch (e) {
                reject({
                    status: false,
                    error: e
                })
            }
        })
    },
    getDetail(_, id) {
        return new Promise(async (resolve, reject) => {
            try {
                const res = await this.$axios.get(`/contact/${id}`)
                const { status, message, data } = res.data
                if (status) {
                    resolve({
                        status,
                        data,
                        error: null
                    })
                } else {
                    reject({
                        status,
                        error: message
                    })
                }
            } catch (e) {
                reject({
                    status: false,
                    error: e
                })
            }
        })
    },
    updateDetail(_, { form, id }) {
        return new Promise(async (resolve, reject) => {
            try {
                const res = await this.$axios.put(`/contact/${id}`, form)
                const { status, message } = res.data
                if (status) {
                    resolve({
                        status,
                        error: null
                    })
                } else {
                    reject({
                        status,
                        error: message
                    })
                }
            } catch (e) {
                reject({
                    status: false,
                    error: e
                })
            }
        })
    },
}