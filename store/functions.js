export const state = () => ({
  step: 1,
  isFilter: false,
  timer: {}
})
export const getters = {
  step(state) {
    return state.step
  },
  isFilter(state) {
    return state.isFilter
  },
  timer(state) {
    return state.timer
  }
}
export const mutations = {
  setStep(state, val) {
    state.step = val
  },
  setFilter(state, val) {
    state.isFilter = val
  },
  setState(state, val) {
    state.timer = val
  },
}

export const actions = {
  fetchTimer({ commit }) {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await this.$axios.get('/time')
        const { status, message, data } = res.data
        if (status) {
          commit("setState", data || {})
          resolve({
            status,
            data,
            error: null
          })
        } else {
          reject({
            status,
            error: message
          })
        }
      } catch (e) {
        reject({
          status: false,
          error: e
        })
      }
    })
  },
  async deleteItems(_, { items, api }) {
    return new Promise(async (resolve, reject) => {
      try {
        let promises = [];
        items.forEach((el) => {
          if (!el) {
            return false;
          }
          let promise = new Promise(async (resolve, reject) => {
            try {
              await this.$axios.delete(`${api}/${el}`)
              resolve({
                status: true,
              });
            } catch (e) {
              reject({
                status: false,
                error: e,
              });
            }
          });
          promises.push(promise);
        });
        await Promise.all(promises);
        resolve({ status: true });
      } catch (error) {
        console.error(error);
      }
    })

  },
}