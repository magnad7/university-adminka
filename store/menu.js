export const state = () => ({
    list: [
        {
            id: 3,
            icon: 'menu-13',
            title: {
                uz: 'Sozlamalar',
                oz: 'Sozlamalar',
                ru: 'Sozlamalar'
            },
            path: '/cabinet/admin',
        },
        {
            id: 4,
            icon: 'menu-3',
            title: {
                uz: 'Yangiliklar',
                oz: 'Yangiliklar',
                ru: 'Yangiliklar'
            },
            path: '/cabinet/admin/news',
        },
        {
            id: 6,
            icon: 'menu-8',
            title: {
                uz: "Bog'lanish",
                oz: "Боғланиш",
                ru: "Контакты",
            },
            path: '/cabinet/admin/contact',

        },
        {
            id: 8,
            icon: 'question',
            title: {
                uz: "Ko'p so'raladigan savollar",
                oz: "Кўп сўраладиган саволлар",
                ru: "Часто задаваемые вопросы",
            },
            path: '/cabinet/admin/faq',

        },
        {
            id: 9,
            icon: 'menu-3-2',
            title: {
                uz: "Banner",
                oz: "Banner",
                ru: "Banner",
            },
            path: '/cabinet/admin/banner',

        },

    ]
})
export const getters = {
    list(state) {
        return state.list
    }
}