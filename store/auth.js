export const state = () => ({
    access: "",
    role: 0,
    user: {}
})

export const getters = {
    role(state) {
        return state.role
    },
    access(state) {
        return state.access
    },
    user(state) {
        return state.user
    }
}

export const mutations = {
    setState(state, { key, value }) {
        value && localStorage.setItem(key, value)
        !value && localStorage.removeItem(key)
        state[key] = value
    },
    setUser(state, val) {
        state.user = val
    }
}

export const actions = {
    userMe({ commit }) {
        return new Promise(async (resolve, reject) => {
            try {
                const res = await this.$axios.get('/me')
                const { status, message, data } = res && res.data || {}
                if (status) {
                    commit("setUser", data)
                    resolve({
                        status,
                        error: null
                    })
                } else {
                    reject({
                        status,
                        error: message
                    })
                }
            } catch (e) {
                reject({
                    status: false,
                    error: e
                })
            }
        })
    },
    login({ commit }, { form }) {
        return new Promise(async (resolve, reject) => {
            try {
                const res = await this.$axios.post('/login', form)
                const { status, message, data } = res && res.data || {}
                if (status) {
                    const { token } = data
                    const token_split = token && token.split(".")
                    const fields_str = token_split && token_split.length > 1 && JSON.parse(window.atob(token_split[1]))
                    //   let {role} = fields_str
                    //   commit("setState", {key: "role", value: role})
                    commit("setState", { key: "access", value: token })
                    resolve({
                        status,
                        error: null
                    })
                } else {
                    reject({
                        status,
                        error: message
                    })
                }
            } catch (e) {
                reject({
                    status: false,
                    error: e
                })
            }
        })
    },
}
