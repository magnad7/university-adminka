export default function ({ redirect }) {
  const role = localStorage.getItem("role")
  if (role != 1) {
    return redirect('/auth/sign-in')
  }
}
