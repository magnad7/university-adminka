export default function ({ route, from, redirect, store }) {
  const access = localStorage.getItem("access")
  const theme = localStorage.getItem("theme")
  // const role = localStorage.getItem("role")

  store.commit("auth/setState", { key: 'access', value: access })
  // store.commit("auth/setState", {key: 'role', value: role})

  if (!access && route.path != "/auth/sign-in" && from != "/auth/sign-in") {
    store.commit("auth/setState", { key: 'access', value: '' })
    // store.commit("auth/setState", {key: 'role', value: ''})
    redirect("/auth/sign-in")
  }
}
