import uz from "./locales/uz.js"
import ru from "./locales/ru.js"
import oz from "./locales/oz.js"
export default {
    // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
    ssr: false,
    server: {
	port: 3037
    },
    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        title: 'University System',
        htmlAttrs: {
            lang: 'en'
        },
        meta: [{
            charset: 'utf-8'
        },
        {
            name: 'viewport',
            content: 'width=device-width, initial-scale=1'
        },
        {
            hid: 'description',
            name: 'description',
            content: ''
        },
        {
            name: 'format-detection',
            content: 'telephone=no'
        }
        ],
        link: [{
            rel: 'icon',
            type: 'image/x-icon',
            href: '/favicon.ico'
        }]
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [
        'ant-design-vue/dist/antd.css',
        'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css',
        '~/assets/fonts/GTWalsheim/stylesheet.css',
        '~/assets/scss',

    ],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [
        '@/plugins/antd-ui',
        '@/plugins/axios',
        '@/plugins/perfect-scrollbar',
        // {
        //     src: "~/plugins/tinymce.js",
        //     mode: "client", // This way the plugin will only be initiated on the client side
        // }
    ],
    router: {
        middleware: ['auth'],
	base: "/admin/"
    },

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: ["@nuxtjs/svg", '@nuxtjs/i18n'],
    i18n: {
        locales: ['uz', 'oz', 'ru'],
        defaultLocale: 'uz',
        detectBrowserLanguage: false,
        strategy: "no_prefix",
        vueI18n: {
            fallbackLocale: 'uz',
            messages: {
                uz,
                oz,
                ru
            }
        }
    },
    svg: {
        vueSvgLoader: {
            // vue-svg-loader options
        },
        svgSpriteLoader: {
            // svg-sprite-loader options
        },
        fileLoader: {
            // file-loader options
        }
    },

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [],

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {}
}
