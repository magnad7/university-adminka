import axios from 'axios'

export default function (context, inject) {
    const instance = axios.create({
        baseURL: "https://mbf-back.mbfacademy.co.uk/api/v1/"
    })
    instance.interceptors.request.use(config => {
        const access = localStorage.getItem("access")
        config.headers["Authorization"] = "Bearer " + access
        return config
    })

    instance.interceptors.response.use(res => {
        return res;
    }, err => {
        if (err.response.status === 401) {
            context.redirect('/auth/sign-in');
        }
    });

    inject('axios', instance)
}